import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class automationFinalExam {
    public static WebDriver driver;
// milena pravi razni neshtak
    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/");
    }

    @AfterMethod
    public void closeBrowser() {
        driver.quit();
    }

    @Test
    public void registrationFormPositiveTest() throws InterruptedException {
        driver.findElement(By.xpath("//div[@id='top-links']//span[text()='My Account']")).click();
        driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown-menu-right']//a[text()='Register']")).click();

       Assert.assertEquals(driver.getTitle(), "Register Account");

        driver.findElement(By.id("input-firstname")).sendKeys("Milena");
        driver.findElement(By.id("input-lastname")).sendKeys("Angelova");
        driver.findElement(By.id("input-email")).sendKeys("milena@abv.bg");
        driver.findElement(By.id("input-telephone")).sendKeys("123456");
        driver.findElement(By.id("input-password")).sendKeys("milena123");
        driver.findElement(By.id("input-confirm")).sendKeys("milena123");

        WebElement subscribeButtonYes = driver.findElement(By.xpath("//div[@class='col-sm-10']//input[@type='radio']"));
        if (!subscribeButtonYes.isSelected())
            subscribeButtonYes.click();

        Assert.assertTrue(subscribeButtonYes.isSelected());

        WebElement checkboxPrivacyPolicy = driver.findElement(By.xpath("//div[@class='pull-right']//input[@name='agree']"));
        if (!checkboxPrivacyPolicy.isSelected())
            checkboxPrivacyPolicy.click();
        Assert.assertTrue(checkboxPrivacyPolicy.isSelected());

        driver.findElement(By.xpath("//div[@class='pull-right']//input[@class='btn btn-primary']")).click();

        Thread.sleep(5000);

        Assert.assertEquals(driver.getTitle(), "Your Account Has Been Created!");
    }


}
